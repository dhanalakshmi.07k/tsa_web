/*var zmq = require('zmq');*/
var express = require('express'),
  config = require('./config/config'),
  glob = require('glob'),
  mongoose = require('mongoose');
var zmqConnector = require('zmqConnector')
var dataSeeder = require('data-seeder');
mongoose.connect(config.db);
var db = mongoose.connection;
db.on('error', function () {
  throw new Error('unable to connect to database at ' +config.db);
});
var models = glob.sync(config.root + '/app/models/*.js');
models.forEach(function (model) {
  require(model);
});
var app = express();

dataSeeder.run({scriptDirPath:config.root+'/app/setUpScripts/',mongo:true}).then(function(){

  var server = require('http').Server(app);
//var socketIoServer = require('socket.io')(server);

  server.listen(config.port, function () {
    console.log('Express server listening on port ' + config.port);
  });

  require('./config/socketIo').init(server )
  require('./config/express')(app,config);
  require('zmqConnector').zmqConnector(__dirname+'/app/dataHandlers/',config.zmq);
})
