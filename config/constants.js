/**
 * Created by Suhas on 8/29/2016.
 */
var constants={
        PROJECT_DETAILS:{
                NAME:'tsa',
                PORT:4300
        },
        socket_PORT_DETAILS:{
                QUEUE_INFO_DATA:'queueInfoData',
                WATCH_LIST_INFO_DATA:'watchListInfoData',
                NOTIFICATION_INFO_DATA:'notificationInfoData'
        }
}

module.exports={constants:constants}