  var path = require('path'),
    rootPath = path.normalize(__dirname + '/..'),
    env = process.env.NODE_ENV || 'development';
  dbHost=process.env.DB_HOST_NAME,
  dbPort=process.env.DB_PORT,
  constants=require('./constants').constants;

var config = {
  development: {
    root: rootPath,
    app: {
      name: constants.PROJECT_DETAILS.NAME
    },
    port: constants.PROJECT_DETAILS.PORT,
    db: 'mongodb://localhost/'+constants.PROJECT_DETAILS.NAME+'-development',/*
    db: 'mongodb://163.172.131.83:28018/smrt',*/
    settingStatus:{
      simulatorStatus:false
    },
    zmq:{
      sendHost:"127.0.0.1",
      recHost:"*",
      //host:'127.0.0.1',
      port:'4201',
      dataHandlerDirectoryPath:"./app/dataAccessModule/saveDataToDb/",
      queue:[
        {name:'queueInfoData',portNo:'4302',type:'pull'},
        {name:'watchListInfoData',portNo:'4303',type:'pull'}]
    }
  },

  integration: {
    root: rootPath,
    app: {
      name: constants.PROJECT_DETAILS.NAME
    },
    port: constants.PROJECT_DETAILS.PORT,/*
    db: 'mongodb://smrt:smrtDevPassw00rd@ds011268.mongolab.com:11268/smrt',*/
    db: 'mongodb://163.172.131.83:28018/'+constants.PROJECT_DETAILS.NAME+'-integration',
    settingStatus:{
      simulatorStatus:true
    },
    zmq:{
      sendHost:"212.47.249.75",
      recHost:"*",
      port:'4201',
      queue:[/*watchListInfoData*/
        {name:'queueInfoData',portNo:'4302',type:'pull'},
        {name:'watchListInfoData',portNo:'4303',type:'pull'}]
    }
  },

  test: {
    root: rootPath,
    app: {
      name: constants.PROJECT_DETAILS.NAME
    },
    port: constants.PROJECT_DETAILS.PORT,
    db: 'mongodb://localhost/'+constants.PROJECT_DETAILS.NAME+'-test',
    zmq:{
      sendHost:"212.47.249.75",
      recHost:"*",
      port:'4201',
      queue:[
        {name:'queueInfoData',portNo:'4302',type:'pull'},
        {name:'watchListInfoData',portNo:'4303',type:'pull'}]
    }
  },

  production: {
    root: rootPath,
    app: {
      name: constants.PROJECT_DETAILS.NAME
    },
    port: constants.PROJECT_DETAILS.PORT,
    db: 'mongodb://localhost/'+constants.PROJECT_DETAILS.NAME+'-production',
    settingStatus:{
      simulatorStatus:false
    },
    zmq:{
      sendHost:"212.47.249.75",
      recHost:"*",
      port:'4201',
      queue:[
        {name:'queueInfoData',portNo:'4302',type:'pull'},
        {name:'watchListInfoData',portNo:'4303',type:'pull'}]
    }
  },

  docker: {
    root: rootPath,
    app: {
      name: constants.PROJECT_DETAILS.NAME
    },
    port: constants.PROJECT_DETAILS.PORT,/*
     db: 'mongodb://smrt:smrtDevPassw00rd@ds011268.mongolab.com:11268/smrt',*/
    db: 'mongodb://'+dbHost+':'+dbPort+'/'+constants.PROJECT_DETAILS.NAME+'-docker',
    settingStatus:{
      simulatorStatus:false
    },
    zmq:{
      sendHost:"localhost",
      recHost:"*",
      queue:[
        {name:'queueInfoData',portNo:'4302',type:'pull',recHost:"*",sendHost:"127.0.0.1"},
        {name:'watchListInfoData',portNo:'4303',type:'pull'}]
    }
  }
};

module.exports = config[env];
