/**
 * Created by zendynamix on 8/31/2016.
 */
NEC.factory("watchLIstService", function ($http,$rootScope){

        var watchLIstServiceData = {
                watchListData:{},
                constants:{
                        WATCH_LIST_INFO_DATA:'watchListInfoData'
                }
        };
        socket.on(watchLIstServiceData.constants.WATCH_LIST_INFO_DATA,function(watchListData){
                setWatchListData(watchListData)
        })
        function setWatchListData(watchListData){
                watchLIstServiceData.watchListData = watchListData;
                $rootScope.$emit(watchLIstServiceData.constants.WATCH_LIST_INFO_DATA,watchListData)
        }
        var getTotalWatchListCountForTerminalAndCheckPoint = function(terminal,checkpoint){
                return $http.get('/watchListInfo/count/'+terminal+'/'+checkpoint);
        }
        var getWatchListData = function(){
                return watchLIstServiceData.watchListData;
        }
        var getTotalWatchListDataForTerminalAndCheckPoint = function(terminal,checkpoint,skip,limit){
                return $http.get('/watchListInfo/data/'+terminal+'/'+checkpoint+'/'+skip+'/'+limit);
        }
        var getTotalWatchListLastDataForTerminalAndCheckPoint = function(terminal,checkpoint){
                return $http.get('/watchListInfo/last/'+terminal+'/'+checkpoint);
        }
        var getTotalWatchListLastDataForTerminalAndCheckPointChart = function(terminal,checkpoint,noOfDocs){
                return $http.get('/watchListInfo/LastData/'+terminal+'/'+checkpoint+'/'+noOfDocs);
        }
        var getHistoricalDataByTerminalId = function(terminalId){
                return $http.get('/watchListInfo/historicalData/'+terminalId+'/'+20)
        }
        return{
                watchLIstServiceData:watchLIstServiceData,
                getWatchListData:getWatchListData,
                getTotalWatchListCountForTerminalAndCheckPoint:getTotalWatchListCountForTerminalAndCheckPoint,
                getTotalWatchListDataForTerminalAndCheckPoint:getTotalWatchListDataForTerminalAndCheckPoint,
                getTotalWatchListLastDataForTerminalAndCheckPoint:getTotalWatchListLastDataForTerminalAndCheckPoint,
                getTotalWatchListLastDataForTerminalAndCheckPointChart:getTotalWatchListLastDataForTerminalAndCheckPointChart,
                getHistoricalDataByTerminalId:getHistoricalDataByTerminalId
        }
})