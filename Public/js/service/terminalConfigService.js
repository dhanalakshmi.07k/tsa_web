/**
 * Created by Suhas on 8/30/2016.
 */

NEC.factory("terminalConfigService", function ($http){
        var getTerminalConfigByTerminalId = function(terminalId){
                return $http.get('/terminalConfig/terminalId/'+terminalId);
        }
        return{
                getTerminalConfigByTerminalId:getTerminalConfigByTerminalId
        }
})