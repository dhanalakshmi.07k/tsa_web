/**
 * Created by zendynamix on 9/1/2016.
 */
NEC.factory("configurationService", function ($http){
        var configurationServiceDetails={
                watchList:{
                        waitTimeThreshold:false
                }
        }
        var getWatchListWaitingTimeThreshold=function(){
                return $http.get('/watchListInfo/getWaitingTimeThreshold')
        }
        var setWatchListWaitingTimeThreshold=function(threshold){
                return $http.get('/watchListInfo/setWaitingTimeThreshold/'+threshold)
        }
        return{
                configurationServiceDetails:configurationServiceDetails,
                getWatchListWaitingTimeThreshold:getWatchListWaitingTimeThreshold,
                setWatchListWaitingTimeThreshold:setWatchListWaitingTimeThreshold
        }
})