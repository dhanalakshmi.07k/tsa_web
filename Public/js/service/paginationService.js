/**
 * Created by SUHAS on 8/31/2016.
 */

NEC.factory("paginationService", function ($q){
        var paginationDetails={
                maxDocPerPage:10,
                maxNoOfPages:10,
                totalDocs:0,
                pageSelected:1,
                noOfPagesToBeDisplayed:10,
                totalNoOfPages:0
        }
        var setPaginationInitDetails=function(maxDocPerPage,maxNoOfPages){
                paginationDetails.maxDocPerPage = maxDocPerPage;
                paginationDetails.maxNoOfPages = maxNoOfPages;
        }
        var setTotalNoOfDocs = function(totalNoOfDocs){
                paginationDetails.totalDocs = totalNoOfDocs;
        }
        var getPaginationDetails = function(){
                /*return new $q(function(resolve,reject){*/
                        paginationDetails.totalNoOfPages = parseInt(paginationDetails.totalDocs) / paginationDetails.maxDocPerPage;
                        var roundedTotalPageNo = Math.round(parseInt(paginationDetails.totalDocs) / paginationDetails.maxDocPerPage);
                        if (paginationDetails.totalNoOfPages > roundedTotalPageNo) {
                                paginationDetails.totalNoOfPages = roundedTotalPageNo + 1;
                        } else if (paginationDetails.totalNoOfPages <1) {
                                paginationDetails.totalNoOfPages = 1;
                        } else if (paginationDetails.totalNoOfPages < roundedTotalPageNo) {
                                paginationDetails.totalNoOfPages = roundedTotalPageNo;
                        }
                        if (paginationDetails.totalNoOfPages <= paginationDetails.maxNoOfPages) {
                                paginationDetails.noOfPagesToBeDisplayed = range(paginationDetails.totalNoOfPages);
                        } else {
                                paginationDetails.noOfPagesToBeDisplayed = range(paginationDetails.maxNoOfPages);
                        }
                        return paginationDetails
              /*  })*/

        }

        function range(n) {
                return new Array(n);
        }
        return{
                setPaginationInitDetails:setPaginationInitDetails,
                setTotalNoOfDocs:setTotalNoOfDocs,
                getPaginationDetails:getPaginationDetails
        }
})