/**
 * Created by Suhas on 8/29/2016.
 */
NEC.factory("dashBoard_Service", function ($http,$rootScope){
        var dashBoardServiceData = {
                recentCheckPointData:'',
                checkPointSelected:null,
                constants:{
                        QUEUE_INFO_DATA:'queueInfoData',
                        CHECK_POINT_NOTIFIER:'checkPointChangeNotifier'
                }
        };
        socket.on(dashBoardServiceData.constants.QUEUE_INFO_DATA,function(checkPointData){
                setCheckPointData(checkPointData)
        })
        function setCheckPointData(data){
                dashBoardServiceData.recentCheckPointData=data;
                $rootScope.$emit(dashBoardServiceData.constants.QUEUE_INFO_DATA,data)
        }

        var getConstants = function(){
                return dashBoardServiceData.constants;
        }
        var getCheckPointData = function(){
                return dashBoardServiceData.recentCheckPointData;
        }
        var setCheckPointSelected = function(data){
                dashBoardServiceData.checkPointSelected =data;
        }
        var getCheckPointSelected = function(){
                return dashBoardServiceData.checkPointSelected;
        }
        return{
                dashBoardServiceData:dashBoardServiceData,
                getCheckPointData:getCheckPointData,
                setCheckPointSelected:setCheckPointSelected,
                getCheckPointSelected:getCheckPointSelected
        }
})