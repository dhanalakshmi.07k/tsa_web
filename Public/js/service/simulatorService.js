/**
 * Created by Suhas on 9/1/2016.
 */
NEC.factory("simulatorService", function ($http,$rootScope){
        var simulatorServiceDetails={
                watchListSimulator:{
                        status:false
                }
        }
        var getSimulatorStatus=function(){
                return $http.get('/watchListInfo/simulatorStatus')
        }
        var setSimulatorStatus=function(status){
                return $http.get('/watchListInfo/simulator/'+status)
        }
        return{
                simulatorServiceDetails:simulatorServiceDetails,
                getSimulatorStatus:getSimulatorStatus,
                setSimulatorStatus:setSimulatorStatus
        }
})
