/**
 * Created by zendynamix on 8/29/2016.
 */
NEC.factory("queueInfoService", function ($http){
        var getLastDataForTerminalAndCheckPoint = function(terminal,checkpoint){
                return $http.get('/queueInfoData/last/'+terminal+'/'+checkpoint)
        }

        var getLastDataForTerminal = function(terminal){
                return $http.get('/queueInfoData/last/'+terminal)
        }
        return{
                getLastDataForTerminalAndCheckPoint:getLastDataForTerminalAndCheckPoint,
                getLastDataForTerminal:getLastDataForTerminal
        }
})