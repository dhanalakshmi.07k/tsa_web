/**
 * Created by zendynamix on 9/1/2016.
 */

NEC.factory("notificationService", function ($http,$rootScope){
        var notificationDetails={
                notificationObj:{},
                constant:{
                        NOTIFICATION_INFO_DATA:'notificationInfoData'
                }
        }
        socket.on(notificationDetails.constant.NOTIFICATION_INFO_DATA,function(notificationObj){
                setNotificationValue(notificationObj)
        })
        function setNotificationValue(notificationObj){
                notificationDetails.notificationObj = notificationObj;
                $rootScope.$emit(notificationDetails.constant.NOTIFICATION_INFO_DATA,notificationObj)
        }
        var clearNotification = function(){
                notificationDetails.notificationArray=[];
        }
        var getNotificationObj = function(){
                return notificationDetails.notificationObj
        }
        return{
                notificationDetails:notificationDetails,
                clearNotification:clearNotification,
                getNotificationObj:getNotificationObj
        }
})