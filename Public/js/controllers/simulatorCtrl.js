/**
 * Created by SUhas on 9/1/2016.
 */

NEC.controller("simulatorCtrl", function ($scope,simulatorService) {
        $scope.simulatorCtrlDetails={
                watchList:{
                        simulatorStatus:false
                }
        }
        function getSimulatorStatus(){
                simulatorService.getSimulatorStatus()
                .then(function(status){
                        $scope.simulatorCtrlDetails.watchList.simulatorStatus = status.data;
                })
        }
        $scope.setSimulatorStatus = function(){
                var status = "stop";
                if($scope.simulatorCtrlDetails.watchList.simulatorStatus){
                        status="start"
                }
                simulatorService.setSimulatorStatus(status)
                .then(function(status){
                        console.log(status.data);
                })
        }
        function init(){
                getSimulatorStatus();
        }
        init();
})