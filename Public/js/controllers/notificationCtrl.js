/**
 * Created by zendynamix on 9/1/2016.
 */
NEC.controller("notificationCtrl", function ($scope,notificationService,$rootScope){
        $scope.notificationDetails={/*notificationDetails.notificationArray*/
                notificationArray:[]
        }
        var deRegisterRootScope1 = $rootScope.$on(notificationService.notificationDetails.constant.NOTIFICATION_INFO_DATA,function(data){
                updateNotificationArray();
        })
        function updateNotificationArray(){
                var notificationObj = notificationService.getNotificationObj();
                if($scope.notificationDetails.notificationArray.length>50){
                        $scope.notificationDetails.notificationArray.pop();
                }
                $scope.notificationDetails.notificationArray.unshift(notificationObj)
        }
        var deRegisterRootScope2;
        $scope.clearNotification = function(){
                $scope.notificationDetails.notificationArray = [];

                deRegisterRootScope2 = $rootScope.$emit("clearNotification",[])
        }

        var deRegisterRootScope3 = $rootScope.$on("clearNotification",function(data){
                $scope.notificationDetails.notificationArray = [];
        })
        $scope.$on('$destroy',function(){
                deRegisterRootScope1();
                deRegisterRootScope2();
                deRegisterRootScope3()
        })

})