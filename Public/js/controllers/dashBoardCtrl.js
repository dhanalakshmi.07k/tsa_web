/**
 * Created by zendynamix on 8/29/2016.
 */
NEC.controller("dashBoardCtrl", function ($scope,dashBoard_Service,
          $rootScope,queueInfoService,terminalConfigService,$q,watchLIstService,
          paginationService,$interval,$state) {


        $scope.dashBordData={/*$scope.dashBordData.queDetails*/
                checkPointSelected:null,
                terminalSelected:1,
                constants:'',
                queueData:[],
                dataReceivedTime:'',
                checkPointsDetails:[],
                paginationDetails:{
                        maxDocPerPage:5,
                        maxNoOfPages:10,
                        totalDocs:0,
                        pageSelected:1,
                        noOfPagesToBeDisplayed:10,
                        totalNoOfPages:0
                },
                watchListData:[],
                queDetails:{
                        ID_VERIFY:[{waitTime:null},{waitTime:null},{waitTime:null}],
                        BAGGAGE_SCAN:[{waitTime:null},{waitTime:null},{waitTime:null}]
                }
        }
        $scope.xAxis= [];
        var yAxisArray = new Array(12);
        yAxisArray.fill(null)
        $scope.yAxis=[
                {
                        name: 'Wait Time',
                        data: yAxisArray

                }/*, {
                        name: 'Medium',
                        data: [5, 11, 11, 13, 20, 8, 15, 11, 8, 12]

                },
                {
                        name: 'High',
                        data: [0, 8, 17, 12, 4, 8, 12, 8, 16, 5]

                }*/];
        $scope.yAxisLine=[
        {
                name: 'CheckPoint A',
                data: yAxisArray

        }, {
         name: 'CheckPoint B',
         data: yAxisArray

         },
         {
         name: 'CheckPoint C',
         data: yAxisArray

         }];
        var deRegisterRoot1 = $rootScope.$on
        (dashBoard_Service.dashBoardServiceData.constants.CHECK_POINT_NOTIFIER,function(data){
                clearData();
                selectCheckPoint();
                getLastData();
                updateCheckPointGraph();
        })
        function clearData(){
                for(var i=0;i<$scope.dashBordData.queDetails.ID_VERIFY.length;i++){
                        $scope.dashBordData.queDetails.ID_VERIFY[i].waitTime = null;
                        $scope.dashBordData.queDetails.BAGGAGE_SCAN[i].waitTime = null;
                }
        }
        function selectCheckPoint(){
                $scope.dashBordData.queueData = [];
                $scope.dashBordData.checkPointSelected = dashBoard_Service.getCheckPointSelected();
                if($scope.dashBordData.checkPointSelected){
                        $scope.dashBordData.watchListData = [];
                        getWatchlistCount();
                }else{
                        setXAxis();
                        setYAxis();
                        getHistoricalDataByTerminal($scope.dashBordData.terminalSelected)
                }
        }
        function getLastData(){
                var terminal = $scope.dashBordData.terminalSelected;
                var checkPointSelected = $scope.dashBordData.checkPointSelected;
                if(!checkPointSelected){

                }else{
                        watchLIstService.getTotalWatchListLastDataForTerminalAndCheckPoint(terminal,checkPointSelected)
                        .then(function(responseData){
                                updateCheckPointData(responseData.data[0])
                                updateDetailsQueueTable(responseData.data[0]);
                        })
                }
        }
        function updateCheckPointData(data){
              /*  var data = watchLIstService.getWatchListData();*/
                var waitTimeDetails={
                        checkPointWaitTime:data.checkPointWaitTime,
                        trend:data.trend
                }
                if(!$scope.dashBordData.checkPointSelected){
                        $scope.dashBordData.dataReceivedTime = data.created_at;
                        var checkPointCode = data.checkPoint;
                        getCheckPointIndexByCheckPoint(checkPointCode)
                        .then(function(index){

                                $scope.dashBordData.queueData[index] = waitTimeDetails;
                        })
                        .catch(function(){});
                }else if($scope.dashBordData.checkPointSelected===data.checkPoint){
                        $scope.dashBordData.dataReceivedTime = data.created_at;
                        $scope.dashBordData.queueData[0] = waitTimeDetails;
                        updateCheckPointGraph();
                }
        }
        function getCheckpointDetails(terminalId){
                terminalConfigService.getTerminalConfigByTerminalId(terminalId)
                .then(function(response){
                        $scope.dashBordData.checkPointsDetails = response.data.checkPoints;
                       /* setYAxis();
                        updateGraph(null)*/
                       /* setXAxis();
                        setYAxis();*/
                })
        }
        function getCheckPointIndexByCheckPoint(checkPointCode){
                return new $q(function(resolve,reject){
                        var checkPointsData = $scope.dashBordData.checkPointsDetails;
                        var counter = 0;
                        for(var i=0;i<checkPointsData.length;i++){
                                counter++;
                                var checkpointObject = checkPointsData[i];
                                if(checkpointObject.code===checkPointCode){
                                        resolve(checkpointObject.order-1)
                                }
                                if(counter==checkPointsData.length){
                                        reject();
                                }
                        }
                })
        }


        /********************watch list*************************/


        var deRegisterRoot2 = $rootScope.$on
        (watchLIstService.watchLIstServiceData.constants.WATCH_LIST_INFO_DATA,function(data){
                updateWatchListData(watchLIstService.watchLIstServiceData.watchListData);
                updateGraph(watchLIstService.watchLIstServiceData.watchListData);
                updateCheckPointData(watchLIstService.watchLIstServiceData.watchListData);
                updateDetailsQueueTable(watchLIstService.watchLIstServiceData.watchListData);
                $scope.$apply();
        })
        function updateDetailsQueueTable(data){
                var zoneDetails = data.zoneID;
                var cameraIndex = data.cameraID;
                var checkPoint = data.checkPoint;
                if($scope.dashBordData.checkPointSelected && $scope.dashBordData.checkPointSelected===checkPoint){
                        $scope.dashBordData.queDetails[zoneDetails][cameraIndex-1].waitTime = data.checkPointWaitTime;
                }
        }
        function updateWatchListData(watchListData){
                /*var watchListData = watchLIstService.getWatchListData();*/
                if(watchListData.checkPoint===$scope.dashBordData.checkPointSelected){
                        var maxPageCount = $scope.dashBordData.paginationDetails.maxDocPerPage;
                        var watchListDataLength = $scope.dashBordData.watchListData.length
                        if(watchListDataLength>=maxPageCount){
                                $scope.dashBordData.watchListData.pop();
                        }
                        $scope.dashBordData.watchListData.unshift(watchLIstService.watchLIstServiceData.watchListData);
                }
        }
        function getWatchlistCount(){
                var terminal = $scope.dashBordData.terminalSelected;
                var checkPoint = $scope.dashBordData.checkPointSelected;
                watchLIstService.getTotalWatchListCountForTerminalAndCheckPoint(terminal,checkPoint)
                .then(function(watchlistDataCount){
                        paginationService.setTotalNoOfDocs(watchlistDataCount.data.count);
                        $scope.dashBordData.paginationDetails = paginationService.getPaginationDetails();
                        getWatchlistDataByPageNo($scope.dashBordData.paginationDetails.pageSelected)
                })
        }
        function getWatchlistDataByPageNo(pageNo){
                var skip = ((pageNo - 1) * $scope.dashBordData.paginationDetails.maxDocPerPage)
                var limit = $scope.dashBordData.paginationDetails.maxDocPerPage;
                var terminal = $scope.dashBordData.terminalSelected;
                var checkPoint = $scope.dashBordData.checkPointSelected;
                        watchLIstService.getTotalWatchListDataForTerminalAndCheckPoint(terminal,checkPoint,skip,limit)
                                .then(function(watchListData){
                                        $scope.dashBordData.watchListData = watchListData.data;
                                })
        }
        $scope.getDataByPageNo=function(pageNo){
                $scope.dashBordData.paginationDetails.pageSelected = pageNo;
                getWatchlistDataByPageNo($scope.dashBordData.paginationDetails.pageSelected);
        }
        $scope.incrementPageForWatchlistData=function(){
                $scope.dashBordData.paginationDetails.pageSelected = $scope.dashBordData.paginationDetails.pageSelected+1;
                getWatchlistDataByPageNo($scope.dashBordData.paginationDetails.pageSelected);
        }
        $scope.decrementPageForWatchlistData=function(){
                $scope.dashBordData.paginationDetails.pageSelected = $scope.dashBordData.paginationDetails.pageSelected-1;
                getWatchlistDataByPageNo($scope.dashBordData.paginationDetails.pageSelected);
        }


        /*******************updating All CheckPoint Graph*************************/
        function setXAxis(){
                var yAxis = [];
                var Time = new Date();
                Time.setSeconds(Time.getSeconds() - 60);
                for (var j = 0; j < 12; j++) {
                        var minute = Time.getMinutes();
                        var seconds = Time.getSeconds();
                        Time.setSeconds(seconds + 5);
                        if (seconds < 10) {
                                seconds = "0" + seconds;
                        }
                        if (minute < 10) {
                                minute = "0" + minute;
                        }
                        var time = Time.getHours() + ":" + minute + ":" + seconds;
                        $scope.xAxis.push(time)
                        /*data.push(0)*/
                };
        }
        function setYAxis(){
                var chart = $("#realTimeGraphAll .flashGraph").highcharts();
                if(chart && chart.series.length==0){
                        var noOfSeries = $scope.dashBordData.checkPointsDetails.length;
                        var array  = new Array(null);
                        array.fill(null);
                        for(var i=0;i<noOfSeries;i++){
                                var checkPointObj  = $scope.dashBordData.checkPointsDetails[i];
                                chart.addSeries({name: checkPointObj.label, data: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]});
                        }
                }

        }
         function updateGraph(data){

                /* var data = watchLIstService.getWatchListData();*/
                var chart = $("#realTimeGraphAll .flashGraph").highcharts();
                if (chart) {
                        var Time = new Date();
                        var seconds = Time.getSeconds();
                        var minute = Time.getMinutes();
                        if (seconds < 10) {
                                seconds = "0" + seconds;
                        }
                        if (minute < 10) {
                                minute = "0" + minute;
                        }
                        var time = Time.getHours() + ":" + minute + ":" + seconds;
                        if(data){
                                updatingSeries(chart,data,time)
                        }else{
                                var noOfSeries = $scope.dashBordData.checkPointsDetails.length;
                                var array  = new Array(12);
                                array.fill(null);
                                for(var i=0;i<noOfSeries;i++){
                                        var checkPointObj  = $scope.dashBordData.checkPointsDetails[i];
                                        chart.addSeries({name: checkPointObj.label, data: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]});
                                }
                        }
                }
                else{
                                console.log("chart not defined")
                }
        }
        function updatingSeries(chartObject,dataObj,time){
                var chart = chartObject;
                var val = dataObj;
                var data = dataObj.checkPointWaitTime;
                getCheckPointIndexByCheckPoint(dataObj.checkPoint)
                .then(function(index){
                        if(chart.series.length==0){
                                setYAxis();
                        }
                        for(var i=0;i<chart.series.length;i++){
                                var chartObj = chart.series[i];
                                if(i==index){
                                        chartObj.addPoint([time, parseInt(data.toFixed(2))], true, true);
                                }else{
                                        chartObj.addPoint([time, chartObj.yData[11]], true, true);
                                }

                        }

                });
        }
        function updateAllDataCheckPointsByHistoricalData(chartObj,data){
                var historicalData = data;
                var checkPointDetails = $scope.dashBordData.checkPointsDetails;
                for(var i=0;i<checkPointDetails.length;i++){
                        var checkPointObj = checkPointDetails[i];
                        var checkPointHistoricalData = historicalData[checkPointObj.code];
                        if(chartObj.series[i]){
                                chartObj.series[i].remove();
                                chartObj.addSeries({name:checkPointObj.label, data: checkPointHistoricalData.yAxis});
                                chartObj.xAxis[0].setCategories(checkPointHistoricalData.xAxis);
                        }else{
                                chartObj.addSeries({name:checkPointObj.label, data: checkPointHistoricalData.yAxis});
                                chartObj.xAxis[0].setCategories(checkPointHistoricalData.xAxis);
                        }
                }
        }

        function getHistoricalDataByTerminal(terminalId){
                watchLIstService.getHistoricalDataByTerminalId(terminalId)
                        .then(function(historicalData){

                                var chart = $("#realTimeGraphAll .flashGraph").highcharts();
                                if(chart){
                                        updateAllDataCheckPointsByHistoricalData(chart,historicalData.data);
                                }else{
                                        setTimeout(function(){
                                                chart = $("#realTimeGraphAll .flashGraph").highcharts();
                                                updateAllDataCheckPointsByHistoricalData(chart,historicalData.data);
                                        },500)
                                }
                        })
        }

        /***********graph data for CheckPoints***********/
        function updateCheckPointGraph(){
                var chart = $("#realTimeGraphCheckPoint .flashGraph").highcharts();
                if($scope.dashBordData.checkPointSelected){
                        if(chart){
                                updateCheckPointYAxisWithHistoricalVal(chart)
                        }else{
                                setTimeout(function(){
                                        chart = $("#realTimeGraphCheckPoint .flashGraph").highcharts();
                                        updateCheckPointYAxisWithHistoricalVal(chart)
                                },500);
                        }

                }
        }

        function updateCheckPointYAxisWithHistoricalVal(chartObj){
                var terminalId = $scope.dashBordData.terminalSelected;
                var checkPointSelected = $scope.dashBordData.checkPointSelected;
                var noOfPoints = 20;
                watchLIstService
                .getTotalWatchListLastDataForTerminalAndCheckPointChart(terminalId,checkPointSelected,noOfPoints)
                .then(function(data){
                        var historicalData = data.data;
                        var yAxis = [];
                        var xAxis = [];
                        for(var i=0;i<historicalData.length;i++){
                                var historicalDataObj = historicalData[i];
                                var Time = new Date(historicalDataObj.created_at);
                                var waitTime = historicalDataObj.checkPointWaitTime;
                                var minute = Time.getMinutes();
                                var seconds = Time.getSeconds();
                                Time.setSeconds(seconds + 5);
                                if (seconds < 10) {
                                        seconds = "0" + seconds;
                                }
                                if (minute < 10) {
                                        minute = "0" + minute;
                                }
                                var time = Time.getHours() + ":" + minute + ":" + seconds;
                                xAxis.unshift(time)
                                yAxis.unshift(waitTime)
                                if(i==historicalData.length-1){
                                        chartObj.series[0].remove();
                                        chartObj.addSeries({name: "Wait time", data: yAxis});
                                        chartObj.xAxis[0].setCategories(xAxis);
                                        chartObj.series[0].color = "red";
                                        chartObj.series[0].update(chartObj.series[0].options);
                                        chartObj.series[0].redraw();

                                }
                        }
                })

        }
        function init(){
                $scope.dashBordData.checkPointSelected = null;
                $state.go('app.checkpoints.allCheckpoint.allCheckpointFlowView')
                /*setXAxis();
                setYAxis();*/
                getCheckpointDetails($scope.dashBordData.terminalSelected);
                paginationService.setPaginationInitDetails($scope.dashBordData.paginationDetails.maxDocPerPage,
                        $scope.dashBordData.paginationDetails.maxNoOfPages);
                if($scope.dashBordData.checkPointSelected) {
                        getWatchlistCount();
                }else{
                      getHistoricalDataByTerminal($scope.dashBordData.terminalSelected)
                }
        }
        init();


        var tick = function() {
                $scope.clock = Date.now();
        }
        tick();
        $interval(tick, 1000);
        $scope.$on('$destroy',function(){
                deRegisterRoot1();
                deRegisterRoot2();
        })

        /*if($stateParams.checkPointType){
                $scope.dashBordData.checkPointSelected = $stateParams.checkPointType;
                clearData();
                selectCheckPoint();
                getLastData();
                updateCheckPointGraph();
                init();
        }*/
})