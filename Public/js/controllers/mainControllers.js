
NEC.controller("mainController", function ($scope,$location,$rootScope,
                               dashBoard_Service,terminalConfigService) {
    $scope.mainCtrlData={
        constants:'',
        terminalSelected:1,
        checkPointDetails:[{label:'-'}],
        checkPointSelected:null
    }
    $scope.currentRout= function (path) {
        var loc=$location.path();
        return loc.includes(path)
    };

    $scope.closePopup= function (popup) {
        $("."+popup).fadeOut(250, function () {
            $(".background").fadeOut(250);
        });
    };

/*    $scope.xAxis= [
        '12:00',
        '13:00',
        '14:00',
        '15:00',
        '16:00',
        '17:00',
        '18:00',
        '19:00',
        '20:00',
        '21:00'
    ];
    $scope.yAxis=[
        {
        name: 'Tokyo',
        data: [0, 8, 17, 12, 4, 8, 12, 8, 16, 5],
        stack: 'a'

    }, {
        name: 'New York',
        data: [5, 11, 11, 13, 20, 8, 15, 11, 8, 12],
        stack: 'a'

    }, {
        name: 'London',
        data: [12, 20, 9, 9, 19, 22, 9, 20, 6, 17],
        stack: 'a'

    },
        {
            name: 'Tokyo',
            data: [0, 8, 12, 14, 7, 11, 14, 11, 9, 10],
            stack: 'b'

        }, {
            name: 'New York',
            data: [8, 15, 9, 11, 11, 15, 11, 15, 12, 11],
            stack: 'b'

        }, {
            name: 'London',
            data: [12, 12, 13, 20, 21, 21, 9, 9, 8, 6],
            stack: 'b'

        },
        {
            name: 'Tokyo',
            data: [12, 20, 17, 11, 23, 21, 17, 13, 7, 12],
            stack: 'c'

        }, {
            name: 'New York',
            data: [10, 12, 16, 20, 7, 14, 12, 5, 11, 8],
            stack: 'c'

        }, {
            name: 'London',
            data: [15, 17, 7, 19, 20, 8, 10, 19, 8, 13],
            stack: 'c'

        }];*/
    $scope.yAxis=[
        {
        name: 'Checkpoint 1',
        data: [0, 8, 17, 12, 4, 8, 12, 8, 16, 5]

    }, {
        name: 'Checkpoint 2',
        data: [5, 11, 11, 13, 20, 8, 15, 11, 8, 12]

    }, {
        name: 'Checkpoint 3',
        data: [12, 20, 9, 9, 19, 22, 9, 20, 6, 17]

    }];
    $scope.setCheckPoint = function(data){
        $scope.mainCtrlData.checkPointSelected=data;
        dashBoard_Service.setCheckPointSelected(data)
        $rootScope.$emit(dashBoard_Service.dashBoardServiceData.constants.CHECK_POINT_NOTIFIER,data)
    }
    function getCheckPointDetailsByTerminal(terminalId){
        terminalConfigService.getTerminalConfigByTerminalId(terminalId)
        .then(function(response){
            $scope.mainCtrlData.checkPointDetails = response.data.checkPoints;
            console.log($scope.mainCtrlData.checkPointDetails)
            /*$scope.mainCtrlData.checkPointSelected = $scope.mainCtrlData.checkPointDetails[0].code;*/
        })
    }
    function init(){
        getCheckPointDetailsByTerminal($scope.mainCtrlData.terminalSelected);
    }
    init();
});



