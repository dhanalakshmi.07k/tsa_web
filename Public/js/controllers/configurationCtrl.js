/**
 * Created by Suhas on 9/1/2016.
 */
NEC.controller("configurationCtrl", function ($scope,configurationService) {
        $scope.configurationCtrlDetails={
                watchList:{
                        threshold:40
                }
        }
        function getWatchListThreshold(){
                configurationService.getWatchListWaitingTimeThreshold()
                .then(function(thresholdRes){
                        $scope.configurationCtrlDetails.watchList.threshold = parseInt(thresholdRes.data.threshold);
                })
        }
        $scope.setConfiguration = function(){
                var threshold =  $scope.configurationCtrlDetails.watchList.threshold;
                configurationService.setWatchListWaitingTimeThreshold(threshold)
                .then(function(thresholdRes){
                        $scope.configurationCtrlDetails.watchList.threshold = parseInt(thresholdRes.data.threshold);
                })
        }
        function init(){
                getWatchListThreshold();
        }
        init();
})