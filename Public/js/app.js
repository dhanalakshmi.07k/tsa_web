/**
 * Created by MohammedSaleem on 11/11/15.
 */

var dependencies = ['ui.router','circularLoaderUI','flashGraphUI','flashCircularGraphUI'];

var NEC = angular.module("NEC", dependencies);

NEC.config(function ($stateProvider, $urlRouterProvider) {
    $stateProvider.state('admin', {
        url: "/admin",
        templateUrl: '../credentials/admin.html'
    })
        .state('admin.signIn', {
            url: "/signIn",
            templateUrl: '../credentials/signIn.html',
            controller:'loginCtrl'
        })
        .state('admin.signUp', {
            url: "/signUp",
            templateUrl: '../credentials/signUp.html'
        })
        .state('app', {
            url: "/app",
            templateUrl: 'templates/app.html'
        })
        .state('app.checkpoints', {
            url: "/checkpoints",
            templateUrl: 'templates/checkpoints.html',
            controller: 'dashBoardCtrl'
        })
        .state('app.checkpoints.checkpoint1Main', {
            url: "/checkpoint1Main",
            templateUrl: 'templates/checkpoint1Main.html'
        })
        .state('app.checkpoints.checkpoint1Main.checkpoint1FlowView', {
            url: "/checkpoint1FlowView",
            templateUrl: 'templates/checkpoint1FlowView.html'
        })
        .state('app.checkpoints.checkpoint1Main.checkpoint1DetailsView', {
            url: "/checkpoint1DetailsView",
            templateUrl: 'templates/checkpoint1DetailsView.html'
        })
        .state('app.checkpoints.allCheckpoint', {
            url: "/allCheckpoint",
            templateUrl: 'templates/allCheckpoint.html'
        })
        .state('app.checkpoints.allCheckpoint.allCheckpointFlowView', {
            url: "/allCheckpointFlowView",
            templateUrl: 'templates/allCheckpointFlowView.html'
        })
        .state('app.checkpoints.allCheckpoint.allCheckpointDetailsView', {
            url: "/allCheckpointDetailsView",
            templateUrl: 'templates/allCheckpointDetailsView.html'
        })



        .state('app.analytics', {
            url: "/analytics",
            templateUrl: 'templates/analytics.html'
        })


        .state('app.settings', {
            url: "/settings",
            templateUrl: 'templates/settings.html'
        })
        .state('app.settings.simulator', {
            url: "/simulator",
            templateUrl: 'templates/simulator.html',
            controller:"simulatorCtrl"
        })
        .state('app.settings.config', {
            url: "/config",
            templateUrl: 'templates/config.html',
            controller:"configurationCtrl"
        })
        .state('app.test', {
            url: "/test",
            templateUrl: 'templates/test.html'
        });

    $urlRouterProvider.otherwise("/admin/signIn");
});


NEC.directive('repeatDone', function () {
    return function (scope, element, attrs) {
        if (scope.$last) { // all are rendered
            scope.$eval(attrs.repeatDone);
        }
    }
});
NEC.filter('secondsToDateTime', function() {
    return function(seconds) {
        if(!seconds){
            return "-";
        }
        else if(seconds>60){
            var d = new Date(0,0,0,0,0,0,0);
            d.setSeconds(seconds);
            var minutes = d.getMinutes();
            var seconds = d.getSeconds();
            return minutes+" min "+seconds+" sec";
        }else{
            return seconds+" sec"
        }
    };
});

NEC.filter('trendToImageName', function() {
    return function(trend) {
        if(!trend){
            return "upPoint.png";
        }else{
            var imageName = trend.toLowerCase();
            imageName = imageName+"Point.png";
            return imageName;
        }
    };
});