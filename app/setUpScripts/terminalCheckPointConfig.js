/**
 * Created by Suhas on 8/30/2016.
 */
var terminalConfigModel = require('../models/terminalCheckPointConfig');
var terminalDetails = [
        {
                'terminalId':1,
                checkPoints:[
                        {'label':'Checkpoint A',code:'A','order':1},
                        {'label':'Checkpoint B',code:'B','order':2},
                        {'label':'Checkpoint C',code:'C','order':3}
                ]
        }
]
function generateTerminalCheckPointConfig(callBack){
        var terminalConfigObj = new terminalConfigModel();
        var generatorCounter = 0;
        for(var i=0;i<terminalDetails.length;i++){
                var terminalObj = terminalDetails[i];
                terminalConfigObj.terminalId = terminalObj.terminalId;
                terminalConfigObj.checkPoints = terminalObj.checkPoints;
                terminalConfigObj.save(function(err,response){
                        generatorCounter++;
                        if(generatorCounter==terminalDetails.length){
                                console.log("Generated All Terminal Configurations")
                                callBack();
                        }
                })
        }
}
module.exports={
        run:function(callBack){
                terminalConfigModel.remove(function(){
                        generateTerminalCheckPointConfig(function(){
                                callBack();
                        })
                })
        }
}