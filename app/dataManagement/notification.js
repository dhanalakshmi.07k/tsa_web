/**
 * Created by Suhas on 9/1/2016.
 */
var constant = require('../../config/constants').constants;
var notificationModel = require('../models/notification');
var socketIo = require('../../config/socketIo').getSocketIoServer();

var saveAndPush = function(data){
        var notificationObj = new notificationModel();
        notificationObj.notificationType = data.type;
        notificationObj.message = data.message;
        notificationObj.save(function(err,result){
                socketIo.emit(constant.socket_PORT_DETAILS.NOTIFICATION_INFO_DATA,notificationObj)
        })
}

module.exports={
        saveAndPush:saveAndPush
}
