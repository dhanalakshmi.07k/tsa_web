/**
 * Created by Suhas on 8/31/2016.
 */

var mongoose = require('mongoose');
var watchListInfoDataModel = require('../models/watchListModel');
var socketIo = require('../../config/socketIo').getSocketIoServer();
var constant = require('../../config/constants').constants;
var random = require('mongoose-simple-random');
var notificationManager = require('./notification');
var moment = require('moment');
var watchListInfoDetails={
        simulator:{
                timerId:'',
                status:false
        },
        threshold:300
};

var startSimulateRandomData = function(){
        if(watchListInfoDetails.simulator.timerId===''){
                var timeDuration = Math.round(Math.floor(Math.random()*3000)+1);
                watchListInfoDetails.simulator.timerId =   setInterval(function(){
                        watchListInfoDataModel.findOneRandom(function(err,result){
                                result.created_at=new Date();
                                result.entryTime.setMinutes(result.entryTime.getMinutes()-Math.round(Math.floor(Math.random() * 5) + 1));
                                result.exitTime.setMinutes(result.entryTime.getMinutes()+Math.round(Math.floor(Math.random() * 3) + 1));
                                delete result._id;
                                save(result);
                                push(result);
                        })
                        timeDuration = Math.round(Math.floor(Math.random()*5000)+1000);
                }, timeDuration);
        }
}
var stopSimulateRandomData = function(){
        /*if(watchListInfoDetails.simulator.timerId=!''){*/
                clearInterval(watchListInfoDetails.simulator.timerId);
                watchListInfoDetails.simulator.timerId='';
      /*  }*/
}
var save = function(data){
        return new Promise(function(resolve,reject){
                if(data){
                        var watchListInfoDataObj  = new watchListInfoDataModel;
                        watchListInfoDataObj.terminal = data.terminal;    // Can be number or letter or string
                        watchListInfoDataObj.checkPoint =data.checkPoint;
                        watchListInfoDataObj.zoneID =data.zoneID;
                        watchListInfoDataObj.capturedPic =data.capturedPic;
                        watchListInfoDataObj.enrolledPic =data.enrolledPic;
                        watchListInfoDataObj.entryTime =new Date(data.entryTime);
                        watchListInfoDataObj.score =data.score;
                        watchListInfoDataObj.exitTime =new Date(data.exitTime);
                        watchListInfoDataObj.waitTime =data.waitTime;
                        watchListInfoDataObj.checkPointWaitTime=data.checkPointWaitTime;
                        watchListInfoDataObj.trend=data.trend;
                        watchListInfoDataObj.cameraID=data.cameraID;
                        if(watchListInfoDataObj.waitTime>watchListInfoDetails.threshold){
                                var notifyObj={
                                        message:"Waiting Time Is " +
                                        ""+watchListInfoDataObj.waitTime+" seconds which is More Than Threshold , At  CheckPoint" +
                                        ""+watchListInfoDataObj.checkPoint,
                                        type:"Watch List"
                                }
                                notificationManager.saveAndPush(notifyObj);
                        }
                        watchListInfoDataObj.save(function(err,data){
                                if(err){
                                        reject(err)
                                }else{
                                        resolve(data)
                                }
                        })
                }
                else{
                        resolve(new Error('Data Is Empty!'))
                }
        })
}
var push = function(obj){
        socketIo.emit(constant.socket_PORT_DETAILS.WATCH_LIST_INFO_DATA,obj)
}
var getAll = function(){
        return new Promise(function(resolve,reject){
                watchListInfoDataModel.find({},function(err,queueInfo){
                        if(err){
                                reject(err)
                        }else{
                                resolve(queueInfo)
                        }
                })
        })
}
var getTotalCountByTerminalAndCheckpoint = function(terminal,checkPoint){
        return new Promise(function(resolve,reject){
                watchListInfoDataModel.count({'terminal':parseInt(terminal),'checkPoint':checkPoint}
                        ,function(err,watchListInfo){
                                if(err){
                                        reject(err)
                                }else{
                                        resolve(watchListInfo)
                                }
                        });
        })
}
var getDataForTerminalAndCheckpointBySkipAndLimit = function(terminal,checkpoint,skip,limit){
        return new Promise(function(resolve,reject){
                watchListInfoDataModel.find({'terminal':terminal,'checkPoint':checkpoint}
                        ,function(err,queueInfo){
                                if(err){
                                        reject(err)
                                }else{
                                        resolve(queueInfo)
                                }
                        }).sort({"created_at":-1}).skip(parseInt(skip)).limit(parseInt(limit))
        })}
var getLastDataByTerminalAndCheckPoint = function(terminal,checkPoint){
        return new Promise(function(resolve,reject){
                watchListInfoDataModel.find({'terminal':parseInt(terminal),'checkPoint':checkPoint}
                        ,function(err,queueInfo){
                                if(err){
                                        reject(err)
                                }else{
                                        resolve(queueInfo)
                                }
                        }).sort({"created_at":-1}).limit(1);
        })
}
var setThreshold = function(threshold){
        watchListInfoDetails.threshold = threshold;
}
var getLastDataForTerminalAndCheckPoint= function(terminal,checkPoint,noOfDocs){
        return new Promise(function(resolve,reject){
                watchListInfoDataModel.find({'terminal':parseInt(terminal),'checkPoint':checkPoint}
                        ,function(err,queueInfo){
                                if(err){
                                        reject(err)
                                }else{
                                        resolve(queueInfo)
                                }
                        }).sort({"created_at":-1}).limit(parseInt(noOfDocs));
        })
}
var getHistoricalDataByTerminalId = function(terminalId,limit){
        return new Promise(function(resolve,reject){
                watchListInfoDataModel.aggregate(
                        [
                                {$match: {"terminal":parseInt(terminalId)}},
                                {$sort: {"created_at":-1}},
                                {$skip: 0},
                                {$limit: parseInt(limit)},
                                {$project: {_id:{"date":"$created_at","checkPoint":"$checkPoint"},"checkPointWaitTime":"$checkPointWaitTime"}}
                        ],function(err,data){
                                if(err){
                                        reject(err.stack)
                                }else{
                                        var checkPointsArray  = data;
                                        var historicalArray = {
                                                A:{
                                                        xAxis:[],
                                                        yAxis:new Array(checkPointsArray.length).fill(null)
                                                },
                                                B:{
                                                        xAxis:[],
                                                        yAxis:new Array(checkPointsArray.length).fill(null)
                                                },
                                                C:{
                                                        xAxis:[],
                                                        yAxis:new Array(checkPointsArray.length).fill(null)
                                                }
                                        }
                                        try{
                                                for(var i=0;i<checkPointsArray.length;i++){
                                                        var checkPointObject = checkPointsArray[i];
                                                        var date = moment(checkPointObject._id.date).format("M/D ,h:mm");
                                                        historicalArray['A'].xAxis[i] = date;
                                                        historicalArray['B'].xAxis[i]  = date;
                                                        historicalArray['C'].xAxis[i]  = date;
                                                        historicalArray[checkPointObject._id.checkPoint].yAxis[i]  = checkPointObject.checkPointWaitTime;
                                                        if(i==checkPointsArray.length-1){
                                                                resolve(historicalArray)
                                                        }
                                                }}
                                        catch(err){console.log(error.stack)}
                                }
                        });
                }
        )
}
module.exports={
        watchListInfoDetails:watchListInfoDetails,
        save:save,
        push:push,
        getAll:getAll,
        startSimulateRandomData:startSimulateRandomData,
        stopSimulateRandomData:stopSimulateRandomData,
        getTotalCountByTerminalAndCheckpoint:getTotalCountByTerminalAndCheckpoint,
        getDataForTerminalAndCheckpointBySkipAndLimit:getDataForTerminalAndCheckpointBySkipAndLimit,
        getLastDataByTerminalAndCheckPoint:getLastDataByTerminalAndCheckPoint,
        setThreshold:setThreshold,
        getLastDataForTerminalAndCheckPoint:getLastDataForTerminalAndCheckPoint,
        getHistoricalDataByTerminalId:getHistoricalDataByTerminalId
}