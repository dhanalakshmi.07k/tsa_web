/**
 * Created by Suhas on 8/29/2016.
 */
var mongoose = require('mongoose');
var queueInfoDataModel = require('../models/queueInfo');
var socketIo = require('../../config/socketIo').getSocketIoServer();
var constant = require('../../config/constants').constants;
var random = require('mongoose-simple-random');
var timerId;

var startSimulateRandomData = function(){
        timerId =   setInterval(function(){
                queueInfoDataModel.findOneRandom(function(err,result){
                        result.created_at=new Date();
                        delete result._id;
                        save(result);
                        push(result);
                })
        }, 3000);
}
var save = function(data){
        return new Promise(function(resolve,reject){
                var queueInfoDataObj  = new queueInfoDataModel;
                queueInfoDataObj.terminal = parseInt(data.terminal);
                queueInfoDataObj.checkPoint = data.checkPoint;
                queueInfoDataObj.queueMetrics = data.queueMetrics;
                queueInfoDataObj.save(function(err,data){
                        if(err){
                                reject(err)
                        }else{
                                resolve(data)
                        }
                })
        })
}
var push = function(obj){
        socketIo.emit(constant.socket_PORT_DETAILS.QUEUE_INFO_DATA,obj)
}
var getAll = function(){
        return new Promise(function(resolve,reject){
                queueInfoDataModel.find({},function(err,queueInfo){
                        if(err){
                                reject(err)
                        }else{
                                resolve(queueInfo)
                        }
                })
        })
}
var getLastDataByTerminalAndCheckPoint = function(terminal,checkPoint){
        return new Promise(function(resolve,reject){
                queueInfoDataModel.find({'terminal':parseInt(terminal),'checkPoint':checkPoint}
                ,function(err,queueInfo){
                        if(err){
                                reject(err)
                        }else{
                                resolve(queueInfo)
                        }
                }).sort({"created_at":-1}).limit(1);
        })
}
var getLastDataOfAllCheckpointByTerminal = function(terminal){
        return new Promise(function(resolve,reject){
                queueInfoDataModel.findOne({'terminal':terminal}
                        ,function(err,queueInfo){
                                if(err){
                                        reject(err)
                                }else{
                                        resolve(queueInfo)
                                }
                        })
        })}


var stopSimulateRandomData = function(){
        clearInterval(timerId)
}
module.exports={
        save:save,
        push:push,
        getAll:getAll,
        getLastDataByTerminalAndCheckPoint:getLastDataByTerminalAndCheckPoint,
        getLastDataOfAllCheckpointByTerminal:getLastDataOfAllCheckpointByTerminal,
        startSimulateRandomData:startSimulateRandomData,
        stopSimulateRandomData:stopSimulateRandomData
}
