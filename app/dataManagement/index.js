/**
 * Created by Suhas on 6/28/2016.
 */
var queueInfo = require('./queueInfo');
var watchListInfo = require('./watchListInfo');
var notification = require('./notification');

module.exports={
    queueInfo:queueInfo,
    watchListInfo:watchListInfo,
    notification:notification
}