/**
 * Created by Suhas on 4/29/2016.
 */
var config = require('../../config/config');
var mongoose = require('mongoose')
var queueInfoDataModel = require('../models/queueInfo');
var config = require('../../config/config');
var queueInfoDataManagement = require('../dataManagement').queueInfo;
mongoose.connect(config.db);
var checkPointArr = ['A','B',"C"];
function saveQueueInfoData(){
    return new Promise(function(resolve,reject){
        var queueInfoDataModelObj = {
            "terminal" : 1,    // Can be number or letter or string
            "checkPoint" : checkPointArr[Math.round(Math.floor(Math.random() * checkPointArr.length-1) + 1)],  // Can be number or letter or string
            "queueMetrics": {
                "queueLength" : Math.round(Math.floor(Math.random() * 90) + 1),  // in feet
                "queueCount" : Math.round(Math.floor(Math.random() * 200) + 1),
                "waitTime" : Math.round(Math.floor(Math.random() * 60) + 0)  // in minutes
            }
        }
        queueInfoDataManagement.save(queueInfoDataModelObj)
        .then(function(result){
            resolve(result)
        })
        .catch(function(err){
            reject(err.stack)
        })
    })

}
function generateQueueInfoData(){
    var counter = 0;
    var num = 10;
    for(var i=0;i<num;i++){
        saveQueueInfoData()
        .then(function(data){
            counter++;
            if(counter==num){
                console.log(num+" Number Of QueueInfoData Generated")
            }
        });
    }
}
generateQueueInfoData();