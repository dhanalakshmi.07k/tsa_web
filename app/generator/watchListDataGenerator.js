/**
 * Created by Suhas on 4/29/2016.
 */
var mongoose = require('mongoose')
var queueInfoDataModel = require('../models/queueInfo');
var config = require('../../config/config');
var watchListInfoDataManagement = require('../dataManagement').watchListInfo;
mongoose.connect(config.db);
var checkPointArr = ['A','B',"C"];
var cameraIDArr = [0,1,2]
var zoneIDArr = ["ID_VERIFY","BAGGAGE_SCAN"];
var trendArr = [ "UP","DOWN","SIDE"];
var imageString = '/9j/4QAYRXhpZgAASUkqAAgAAAAAAAAAAAAAAP/sABFEdWNreQABAAQAAAA8AAD/4QMraHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wLwA8P3hwYWNrZXQgYmVnaW49Iu+7vyIgaWQ9Ilc1TTBNcENlaGlIenJlU3pOVGN6a2M5ZCI/PiA8eDp4bXBtZXRhIHhtbG5zOng9ImFkb2JlOm5zOm1ldGEvIiB4OnhtcHRrPSJBZG9iZSBYTVAgQ29yZSA1LjAtYzA2MCA2MS4xMzQ3NzcsIDIwMTAvMDIvMTItMTc6MzI6MDAgICAgICAgICI+IDxyZGY6UkRGIHhtbG5zOnJkZj0iaHR0cDovL3d3dy53My5vcmcvMTk5OS8wMi8yMi1yZGYtc3ludGF4LW5zIyI+IDxyZGY6RGVzY3JpcHRpb24gcmRmOmFib3V0PSIiIHhtbG5zOnhtcD0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wLyIgeG1sbnM6eG1wTU09Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9tbS8iIHhtbG5zOnN0UmVmPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvc1R5cGUvUmVzb3VyY2VSZWYjIiB4bXA6Q3JlYXRvclRvb2w9IkFkb2JlIFBob3Rvc2hvcCBDUzUgTWFjaW50b3NoIiB4bXBNTTpJbnN0YW5jZUlEPSJ4bXAuaWlkOkI2MURGNDYyNUFFNTExRTZBMjA2QjYyMjJGMDc1NUNGIiB4bXBNTTpEb2N1bWVudElEPSJ4bXAuZGlkOkI2MURGNDYzNUFFNTExRTZBMjA2QjYyMjJGMDc1NUNGIj4gPHhtcE1NOkRlcml2ZWRGcm9tIHN0UmVmOmluc3RhbmNlSUQ9InhtcC5paWQ6QjYxREY0NjA1QUU1MTFFNkEyMDZCNjIyMkYwNzU1Q0YiIHN0UmVmOmRvY3VtZW50SUQ9InhtcC5kaWQ6QjYxREY0NjE1QUU1MTFFNkEyMDZCNjIyMkYwNzU1Q0YiLz4gPC9yZGY6RGVzY3JpcHRpb24+IDwvcmRmOlJERj4gPC94OnhtcG1ldGE+IDw/eHBhY2tldCBlbmQ9InIiPz7/7gAOQWRvYmUAZMAAAAAB/9sAhAAGBAQEBQQGBQUGCQYFBgkLCAYGCAsMCgoLCgoMEAwMDAwMDBAMDg8QDw4MExMUFBMTHBsbGxwfHx8fHx8fHx8fAQcHBw0MDRgQEBgaFREVGh8fHx8fHx8fHx8fHx8fHx8fHx8fHx8fHx8fHx8fHx8fHx8fHx8fHx8fHx8fHx8fHx//wAARCABaAFoDAREAAhEBAxEB/8QAlAAAAgMBAQEAAAAAAAAAAAAABgcDBQgEAQIBAAIDAQAAAAAAAAAAAAAAAAABAgMEBRAAAQIEBAAKBgYIBwEAAAAAAQIDABEEBSExEgZBUWFxIjITMxQHkaFCUiM0gbFiUyQWwdFykkNEFTXhspRFVTYIFxEAAgMAAgMAAwEBAAAAAAAAAAERAgMhMUESBFETFDJx/9oADAMBAAIRAxEAPwBPP1T/AG7vxFYKVhqPHDA8FS994v8AeMAHqaqo1d4v94w0BY2cv1F2pmi4vTqBV0jlOG3AJNmnrLfPC2xFPTU61LDYCVudAZZyzim30VRfT5bsWt88vKOtrqitqbm8KipWpaglWAJ4uQRT/Umy/wDiaQHXbYt4odTlI+uqYHCCZ4c0X12TKL4NAo/UVKFFClrSoGRBJi2ShqCMVT5HeL/eMAHqap/WPiLl+0YAOpytfSypXaKwGHSOcAjt8HWe+v8At3a9Y9aAAXfWe3d/bV9cRGfAcVABZUFMpwpMpkmSBxmH0pYJS4HJ5beX9PRtC8XFvW+5iwk8HLKOdvvJ1Pn+dIYFSSUYHjAkMJRhtaezeuOikrKUlE9XSxlyQpCCiRUP0781YonLj9Macto7KtMpB3fm1KOtolXa3p0VbeL7SeqtPvD7QjfW/k5uuYrzNJ0nAiL0zH0etnHjiQHS20p51inGJdWJ8wgEOH8oq9z/AGn1wAJGoY+O5h7avriIz1FJp6bvRSMZQ4AMfLizrvN3mU/AaKUtzymTNR9Aind8QX/OuZNDLo1NhDKUSQgJSiXEBHN0q5Ovm1BL4DUjQoYcRiP6xu5W3C1OIZUoCYTEHk0iddEwPqWCUrJEgCZnmiKLGVaqwJaW2o4HAp45xuytwYdqcit3LRJpbisI7tRJTzHERsytKOZtWGV7IBUBxxcikJti2s3LdLDYE0NkD9JgA0t4Cn4h3HYf4QAZrVsbeLzy/DWaoVNRkrTLM8sAy9sPkVvS6PJVcGfCMTEwozVL6IIFIw6DadNtC8MUDSeqylxSzwqXMT9UZ9+zV84eNvJUhC5Aq4TGSzN9ToLzujopTLPVCkGinrXHlIXNXDKQiqzLEgSvDSkU/RT3hJEQiS1MBbktSHTLIHCLaMr0A7eCwupTxiWEbsTlblLSjpKWckCcaEZhteRdl7SpduDicBNU4YB9+aGve/n+x+iABo21dK8ylbaUyOOESEWSEo4gBCAVO461V2r2LqWuxp3vwzcxJXQWQCrn4I5z29zqfz/r4mS8bS1T04K1hDaAAFKMvWYraLkz4Vd7OmQ8W1PgGoQpQFdWV9GVKcn0cCrSZggmU4ptZFiBu/1jWoaFTCQcDKcJEugJrqftHxkSo9KWUXZrkrvbgCtw0ofrn0NuJSWQJhRlqIE9I5eKNtLJdmDTN2lrwUrbZ8KAOs8oAcxjSYzSHllaRbNoqfKZKU2VeqABVf1pz3v5rxEADu8st1CrYNI6v4rZIhoQzGlBQHLDAXdWx4lFYysBumZfQhluWKSlyer6Y464szuX/wAoj3HZFXWkNH2xp219ZxGK5cOmfHDYq8cimv8A5TMrvQNCa5xso0ltJXpDn3mufqiVrfhDrmplsNl7coNv7Gq2vivVSkhAqHVEr1Z5zyEUWRPlsTbG3bzckVFa5fX6VYJDKChTiZjHpEGePBGitkvBTbNvyS2qhu9PWNrdqFVTKSCt9QUn1GJey8EPRorL+QmvqKWRStt3xIV7+rAeicSqvayQvb0o3+TyzUBrL3RUSBMJIKhymNxyzVFBbk0+23GEjBFOr/LABmDtD6pwwCPYu6V0lwTUJXLSshwck4EBp3b91arqJp5CgdQBhiKrclhrXDUPME+GUhSpIlNKpTJIzzyjn74OfZHRw+irr6vsqbfemKilZWcHFJGqYxnLGM9nDNNOUfNTfWmHQ0MVrIShIlMk5QPQuWfk+t0sodsLyHEAgAlUzyZiC3RCv+hI2O4oaub9Avo6060A5GRIwiN1CLKcsmu1xQkpQ4oBKTgBxccPOrkjtZIC7k4w/W65DxDiytxU5lLQ6g5J5xvxr5OZ9F+FVBX5P2k3Dci6tQmhtWB5sI0GQ01T0oXQuty66CgfSJQAZe/Ktf8Adn+5eAy9qGABWq5qpLisE9BS1A+mIpgaJ8pN3jo0Ly8DLsyTwRIQ5nHkKo3SeqpBHpEoVlwOr5Qkk1S6OvSTPs+klueMsZRyro6tXBJb73TiqcqVIU5VLCtMxq7NE5S5zCpRFttWwQ3UbwWalm31D9A0UFakNkqQVqPAkzzGcos9akHawuaRVwt1WzUvOh6pCgSpSpGWMxjEr19iFNHU671d0VTqS0TpcQVEn2YMqwR209mU1AOzpHnuFWCecx0Ec0f3kbYvD2jxSk9J3IwAO2mb0tpHJDEU/wCUbd92PnfHZfxOOADC9S2tFS4R76vriBILtnbmeo6hperStpQkYkJmjX98Ke2O7VUaVP1ZZJbabE1FUsB6YkIDK5yoR0n21Dtwl1KTMKSogFSSOAgxynXk6rfH/SGxs22prn116e9OhtIWpOAzPRIziL4J52ODdSNqNuJpUCpBBJIQ8pKZnjnOJq0+C/8AZHYuL3TWdJWtKniGz8JK1kgq4Jgw02zNr69kWz7QncO4WbcdTbDiFl1aMkIQkyIn9qUaaV5MVrcEbNCo1LVtT0lofU07L3m1aSI0GeDVuw7Smis9IwBKSRP0QwDVsSEAiT9UIYrar/zrsV9alFhaVKJJIWeGGI4HP/M2z5ksu1DR4w5+uAcnTQ2bbmx2jTU1Wu5PezTagvSftKGAgAl8wbP+OZqkgtprm0vJKcgvSAofRGDdet5/J0/navnH4Fc4+5R3B2ncVIjpJmMZHMgxGylEE2mfNa9ZalxLjykqUlBJUZ9YDLniqGuC9WTFrfrmurq+xYQCVHQ0hIxJOUo050gy63kYewLZT7eoHap9QVVrRqfc4EoQNWhP6Y01UGdi8tN7foa/+opAU65UKqCheI6SyqR+iGmVmrPLPf8At7c9IhFI8inuTaQHre4oBYMs259dPNEhDBExgRKGI+5/VCGDl68wdv20KQ274ypGAaZxE+VeQhiF9ed6328KUlx7w1KcE0zJKRL7SszCGVLDeJlhmTEgHVX2Ri8WFimd6DqW0LYelMoWEjHmORinWisoLcdXRyhH7u2vUhbtBV05FWyr4WnBUlZKbUOskxhl1cM6DVbqUDTXkNvJ9ldbXv8AgKVSPw6VAuPqUctaEy0J5TjyRspnKMV7w4TBlWyqvbdy7Ota/EEdF8nUFDjSeCJesEU5OzctzNv20sDv66bLPIj21ejCJEbC4M5JAnxShEQotNpVSFioWtSasq1ApJSUYTEiOGGhDO23517ts5bp65SbrRpkNL+DoTyOjH0ziUhAff8A3zbv/H1Xy/bZo6/uc32oJFAIq6x7rPgyzgGTNZ/wsuGARL7C+66vBEgH1RfKs5d2nLLqiIMECd//AO82v5ju05/L957P2/e5Irt2ui2nT7CiozV1cjn+mL0UiF8xv74r5XrDr9SI2LKi08zPmrf8v3Ss+7zHUiLEwWtfz7HyfXHWyhCDBzvW/lszzwwIH+ufl8oYzq/0/wAvCA//2Q=='
function saveQueueInfoData(){
    return new Promise(function(resolve,reject){
        var queueInfoDataModelObj = {
            "terminal" : 1,    // Can be number or letter or string
            "checkPoint" : checkPointArr[Math.round(Math.floor(Math.random() * checkPointArr.length-1) + 1)],  // Can be number or letter or string
            "zoneID":zoneIDArr[Math.round(Math.floor(Math.random()*zoneIDArr.length-1)+1)],
            "capturedPic":imageString,
            "enrolledPic":imageString,
            "entryTime": new Date(),
            "exitTime": new Date(),
            "score":Math.round(Math.floor(Math.random()*100)+1),
            "waitTime":Math.round(Math.floor(Math.random()*400)+1),
            "checkPointWaitTime":Math.round(Math.floor(Math.random()*400)+1),
            "trend":trendArr[Math.round(Math.floor(Math.random()*trendArr.length-1)+1)],
            "cameraID":Math.round(Math.floor(Math.random()*3)+1)
        }
        queueInfoDataModelObj.entryTime.setMinutes(Math.round(Math.floor(Math.random() * 59) + 1));
        queueInfoDataModelObj.exitTime.setMinutes(queueInfoDataModelObj.entryTime.getMinutes()+Math.round(Math.floor(Math.random() * 59) + 1));
        watchListInfoDataManagement.save(queueInfoDataModelObj)
        .then(function(result){
            resolve(result)
        })
        .catch(function(err){
            reject(err.stack)
        })
    })

}
function generateQueueInfoData(){
    var counter = 0;
    var num = 100;
    for(var i=0;i<num;i++){
        saveQueueInfoData()
        .then(function(data){
            counter++;
            if(counter==num){
                console.log(num+" Number Of QueueInfoData Generated")
            }
        }).catch(function(err){console.log(err)});
    }
}
generateQueueInfoData();