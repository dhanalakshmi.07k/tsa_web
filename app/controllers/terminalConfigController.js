/**
 * Created by Suhas on 8/30/2016.
 */
var express = require('express'),
        router = express.Router(),
        mongoose = require('mongoose');
var config = require('../../config/config');/*
var queInfoDataManagement = require('../dataManagement').queueInfo;*/
var terminalConfigModel = mongoose.model('terminalConfigModel');
module.exports = function (app) {
        app.use('/', router);
};

router.get('/terminalConfig/terminalId/:terminalId',function(req,res){
        terminalConfigModel.findOne({'terminalId':parseInt(req.params.terminalId)},function(err,data){
                if(err){
                        res.status(406).send(err.stack)
                }else{
                        res.send(data)
                }
        })
})