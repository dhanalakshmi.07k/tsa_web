/**
 * Created by zendynamix on 8/29/2016.
 */
var express = require('express'),
   router = express.Router();
var constants = require('../../config/constants');
module.exports = function (app) {
        app.use('/', router);
};

router.get('/constants',function(req,res){
       res.send(constants)
})