/**
 * Created by Suhas on 6/28/2016.
 */
var express = require('express'),
    router = express.Router(),
    mongoose = require('mongoose');
var config = require('../../config/config');
var queInfoDataManagement = require('../dataManagement').queueInfo;
var queueInfoDataModel = mongoose.model('queueInfoModel');
module.exports = function (app) {
    app.use('/', router);
};

router.get('/queueInfoData/last/:terminal/:checkPoint',function(req,res){
    queInfoDataManagement.getLastDataByTerminalAndCheckPoint(req.params.terminal,req.params.checkPoint)
    .then(function(data){
        res.send(data)
    }).catch(function(err){
        res.status(406).send(err.stack)
    })
 })
function startSimulateData(){
    queInfoDataManagement.startSimulateRandomData();
}

function stopSimulateData(){
    queInfoDataManagement.startSimulateRandomData();
}
/*startSimulateData();*/
