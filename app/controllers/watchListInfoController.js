/**
 * Created by zendynamix on 8/31/2016.
 */
var express = require('express'),
        router = express.Router(),
        mongoose = require('mongoose');
var config = require('../../config/config');
var watchListInfoDataManagement = require('../dataManagement').watchListInfo;
module.exports = function (app) {
        app.use('/', router);
};
var watchlistData = {
        simulator:{
                        start:function(){
                                if(watchListInfoDataManagement.watchListInfoDetails.simulator.timerId==''){
                                        watchListInfoDataManagement.startSimulateRandomData();
                                        return "Simulator started Successfully";
                                }else{
                                        return "Simulator Already Started";
                                }

                        },
                        stop: function(){
                                if(watchListInfoDataManagement.watchListInfoDetails.simulator.timerId!=''){
                                        watchListInfoDataManagement.stopSimulateRandomData();
                                        return "Stopped Successfully";
                                }else{
                                        return "Simulator Is Already Stopped";
                                }
                        },
                        status:false
                }
        }
router.get('/watchListInfo/count/:terminal/:checkPoint',function(req,res){
        watchListInfoDataManagement.getTotalCountByTerminalAndCheckpoint(req.params.terminal,req.params.checkPoint)
                .then(function(count){
                        var data = {
                                count:count
                        }
                        res.send(data)
                }).catch(function(err){
                res.status(406).send(err.stack)
        })
})
router.get('/watchListInfo/data/:terminal/:checkPoint/:skip/:limit',function(req,res){
        watchListInfoDataManagement.
        getDataForTerminalAndCheckpointBySkipAndLimit(req.params.terminal,req.params.checkPoint,
        req.params.skip,req.params.limit)
                .then(function(data){
                        res.send(data)
                }).catch(function(err){
                res.status(406).send(err.stack)
        })
})
router.get('/watchListInfo/last/:terminal/:checkPoint',function(req,res){
        watchListInfoDataManagement.getLastDataByTerminalAndCheckPoint(req.params.terminal,req.params.checkPoint)
                .then(function(data){
                        res.send(data)
                }).catch(function(err){
                res.status(406).send(err.stack)
        })
})

router.get('/watchListInfo/simulator/:status',function(req,res){
        var message = watchlistData.simulator[req.params.status]();
        res.send(message)
})
router.get('/watchListInfo/simulatorStatus',function(req,res){
        if(watchListInfoDataManagement.watchListInfoDetails.simulator.timerId==''){
                res.send(false)
        }else{
                res.send(true)
        }
});

router.get('/watchListInfo/setWaitingTimeThreshold/:threshold',function(req,res){
        watchListInfoDataManagement.setThreshold(parseInt(req.params.threshold));
        var data = {
                threshold:watchListInfoDataManagement.watchListInfoDetails.threshold
        }
        res.send(data)
});
router.get('/watchListInfo/getWaitingTimeThreshold',function(req,res){
        var data = {
                threshold:watchListInfoDataManagement.watchListInfoDetails.threshold
        }
        res.send(data)
});

router.get('/watchListInfo/LastData/:terminal/:checkPoint/:noOfDoc',function(req,res){
        watchListInfoDataManagement
                .getLastDataForTerminalAndCheckPoint(req.params.terminal,req.params.checkPoint,req.params.noOfDoc)
                .then(function(data){
                        res.send(data)
                })
                .catch(function(err){
                        res.status(406).send(err.stack)
                })
});


router.get('/watchListInfo/historicalData/:terminal/:limit',function(req,res){
        watchListInfoDataManagement
        .getHistoricalDataByTerminalId(req.params.terminal,req.params.limit)
        .then(function(data){
                res.send(data)
        })
        .catch(function(err){
                res.status(406).send(err.stack)
        })
});