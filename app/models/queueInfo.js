/**
 * Created by Suhas on 8/29/2016.
 */
var mongoose = require('mongoose'),
    Schema = mongoose.Schema;
var random = require('mongoose-simple-random');

var queueInfoSchema = new mongoose.Schema(
    {
        "terminal":Number,
        "checkPoint":String,
        "queueMetrics":{
            "queueLength":Number,
            "queueCount":Number,
            "waitTime":Number
        },
        created_at:{type:Date},
        updated_at:{type:Date}
    },{collection: "queueInfoModel"});
queueInfoSchema.plugin(random);
queueInfoSchema.index({'data.personOfInterest.location':1});
queueInfoSchema.pre('save', function(next){
        var now = new Date();
        this.updated_at = now;
        if (!this.created_at ) {
                this.created_at = now;
        }
        next();
});

queueInfoSchema.post('save', function(doc) {/*
        console.log(' Data Received For Terminal %s and CheckPoint %s has been Saved and pushed', doc.terminal,doc.checkPoint);*/
});
module.exports =mongoose.model('queueInfoModel',queueInfoSchema);
