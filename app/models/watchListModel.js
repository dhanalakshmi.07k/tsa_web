/**
 * Created by SUHAS on 8/31/2016.
 */
var mongoose = require('mongoose'),
   Schema = mongoose.Schema;
var random = require('mongoose-simple-random');

var watchListInfoSchema = new mongoose.Schema(
        {
                "terminal":Number,
                "checkPoint":String,
                "cameraID":Number,
                "zoneID":String,
                "capturedPic":String,
                "enrolledPic":String,
                "score":Number,
                "entryTime":Date,
                "exitTime":Date,
                "waitTime":Number,
                "checkPointWaitTime":Number,
                "trend":String,
                created_at:{type:Date},
                updated_at:{type:Date}
        },{collection: "watchListInfoDoc"});
watchListInfoSchema.plugin(random);
watchListInfoSchema.index({'terminal':1});
watchListInfoSchema.index({'checkPoint':1});
watchListInfoSchema.pre('save', function(next){
        var now = new Date();
        this.updated_at = now;
        if (!this.created_at ) {
                this.created_at = now;
        }

        next();
});

watchListInfoSchema.post('save', function(doc) {/*
        console.log(' watch List Data Received For Terminal %s and CheckPoint %s has been Saved and pushed', doc.terminal,doc.checkPoint);
*/});
module.exports =mongoose.model('watchListInfoModel',watchListInfoSchema);