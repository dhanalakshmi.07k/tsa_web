/**
 * Created by Suhas on 9/1/2016.
 */

var mongoose = require('mongoose'),
        Schema = mongoose.Schema;
var notificationSchema = new Schema({
        level:String,
        message: String,
        timeStamp:Date,
        notificationType:String,
        created_at:{type:Date},
        updated_at:{type:Date}
},{collection: "notification"})
module.exports = mongoose.model('notification',notificationSchema);

notificationSchema.pre('save', function(next){
        var now = new Date();
        this.updated_at = now;
        if (!this.created_at ) {
                this.created_at = now;
        }

        next();
});