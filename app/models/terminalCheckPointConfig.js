/**
 * Created by Suhas on 8/30/2016.
 */
var mongoose = require('mongoose'),
        Schema = mongoose.Schema;
var terminalConfigSchema = new mongoose.Schema({
        terminalId:Number,
        checkPoints:Array,
        created_at:{ type: Date},
        updated_at:{ type: Date}
},{
        collection:"terminalConfigDoc"
});
terminalConfigSchema.pre('save', function(next){
        var now = new Date();
        this.updated_at = now;
        if ( !this.created_at ) {
                this.created_at = now;
        }
        next();
});
terminalConfigSchema.post('save', function(doc) {
        console.log('Configuration For Terminal of %s  has been saved', doc.terminalId);
});
module.exports = mongoose.model('terminalConfigModel',terminalConfigSchema);