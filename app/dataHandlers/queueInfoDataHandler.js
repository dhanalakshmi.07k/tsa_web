/**
 * Created by Suhas on 8/29/2016.
 */
var queueInfoDataManagement = require('../dataManagement').queueInfo;
var saveData = function(data){
        queueInfoDataManagement.save(data)
        .then(function(savedData){
                queueInfoDataManagement.push(savedData);
        })
}

module.exports={
        saveData:saveData
}