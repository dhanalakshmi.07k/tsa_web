/**
 * Created by Suhas on 6/28/2016.
 */
var queueInfoDataHandler = require('./queueInfoDataHandler.js');
var watchListInfoDataHandler = require('./watchListInfoDataHandler.js');
module.exports = {
    queueInfoDataHandler:queueInfoDataHandler,
    watchListInfoDataHandler:watchListInfoDataHandler
}